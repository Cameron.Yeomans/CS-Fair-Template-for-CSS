<nav class="navbar navbar-default" role="navigation">
    <div class="container-fluid">

        <!-- mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ol class="nav navbar-nav">
                <?php
                print '<li';
                if (basename(PHP_SELF) == "index-2.php") {
                    print ' class="active"';
                }
                print '><a href="' . BASE_PATH . 'index-2.php">Home</a></li>';

                print '<li';
                if (basename(PHP_SELF) == "about-2.php") {
                    print ' class="active"';
                }
                print '><a href="' . BASE_PATH . 'about-2.php">About</a></li>';

                print '<li';
                if (basename(PHP_SELF) == "judges-2.php") {
                    print ' class="active"';
                }
                print '><a href="' . BASE_PATH . 'judges-2.php">Judges</a></li>';

                print '<li';
                if (basename(PHP_SELF) == "sponsors-2.php") {
                    print ' class="active"';
                }
                print '><a href="' . BASE_PATH . 'sponsors-2.php">Sponsors</a></li>';

                print '<li';
                if (basename(PHP_SELF) == "projects-2.php") {
                    print ' class="active"';
                }
                print '><a href="' . BASE_PATH . 'projects-2.php">Projects</a></li>';

                print '<li';
                if (basename(PHP_SELF) == "schedule-2.php") {
                    print ' class="active"';
                }
                print '><a href="' . BASE_PATH . 'schedule-2.php">Schedule</a></li>';

                print '<li';
                if (basename(PHP_SELF) == "photos-2.php") {
                    print ' class="active"';
                }
                print '><a href="' . BASE_PATH . 'photos-2.php">Gallery</a></li>';
                ?>
            </ol>
        </div>
        <!-- /.navbar-collapse -->
    </div>
</nav>
