<h2>Rules</h2>
<ol>
    <li>All Participants must have been enrolled in a Computer Science 
        course at UVM or be an active CS or Data Science, Major or Minor during the <?php echo FAIR_YEAR; ?> calendar year (Jan to Dec).</li>
    <li>Projects
        <ul>
            <li>Group or individual projects are allowed.</li>
            <li>Course projects are allowed (the more unique they are from your classmates' the higher your chances of getting in and winning).</li>
            <li>Non course projects are highly encouraged  (i.e., something you did on your own).</li>
            <li>You may enter more than one project but you cannot present more than one project in a time slot. It is your responsibility to confirm you have not been assigned to present more than one project at a time.</li>
            <li>Registration does not guarantee project will be accepted.</li>
        </ul>
    </li>
    <li>All entries must be present at their booth. We will provide space on a table, but you need to provide a laptop (with a good battery -- no power is available) and/or other display (e.g., poster).</li>
    <li>You are eligible to win only one competitive prize, you may forfeit a lower prize.</li>
    <li>Unclaimed prizes will be forfeited after one week after the fair.</li>
    <li>All entries allow us to use photos for promotional material.</li>
    <li>All entries must be received by November 12<sup>th</sup> in 
        order to receive a t-shirt. T-Shirts not picked up before Thanksgiving Break are forfeited to a first come first serve.</li>
    <li>Monetary prizes will be in the form of UVM gift card.</li>
    <li>UVM monetary <a target="_blank" href="http://www.uvm.edu/policies/acct/prizes.pdf">prize policy</a> and <a target="_blank" href="http://www.uvm.edu/policies/acct/scholarships.pdf">scholarship policy</a>.
    </li>
</ol>
