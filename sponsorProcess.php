<div class="row">
    <div class="col-md-6">
        <h3>Information</h3>
        <p>Sponsors will have their company logos posted on the CS Fair website and will be given table space (if requested) at the Fair to display their company information and paraphernalia. Sponsors are encouraged to send at two or more representatives to act as a judges. Many sponsors send a person to just walk around. Sponsorship is free, but we do welcome donations of any amount. We cannot have the fair without help.</p>
    </div>
    <div class="col-md-6">
        <h3>Process</h3>
        <ol>
            <li class="complete">Fill out required fields.</li>
            <li class="complete">Submit form</li>
            <li>Check mail and confirm sponsorship</li>
            <li>We will contact you and approve your submission. At this point your data will be made  live on the web site.</li>
            <li>We will send a link to update your information.</li>
            <li>You can then update your judges and other information.</li>
            <li>Join us at the Fair!</li>
        </ol>   
    </div>
</div>
