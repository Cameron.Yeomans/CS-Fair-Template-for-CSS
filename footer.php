<footer class="col-md-12">
    <p>Web design by:<span id="author"></span></p>
    <p>Event coordination provided by the Society of UVM Women in Computer Science</p>
    <p>University of Vermont, Computer Science Department, 351 Votey, (802) 656 - 3330</p>
    <p class="fair-links">
        <a href="//csfair.w3.uvm.edu/2013/">CS Fair 2013</a> 
        | <a href="//csfair.w3.uvm.edu/2014/">CS Fair 2014</a> 
        | <a href="//csfair.w3.uvm.edu/2015/">CS Fair 2015</a> 
        | <a href="//csfair.w3.uvm.edu/2016/">CS Fair 2016</a>
    </p>
    <p class="admin-link">[<a href="">Admin</a>]</p>
</footer>
