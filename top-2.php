<?php
include "lib/constants.php";
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title><?php echo FAIR_YEAR; ?> UVM CS Fair</title>
        <meta charset="utf-8">
        <meta name="author" content="Society of Women in Computer Science, Robert M. Erickson">
        <meta name="description" content="A Showcase of Student Projects held once a year in decemeber during the last week of classes. Students compete for prizes in several categories, best overall, best in class and my favorite random!">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link class="jsbin" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/themes/base/jquery-ui.css" rel="stylesheet" type="text/css" />
        <script class="jsbin" src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
        <script class="jsbin" src="//ajax.googleapis.com/ajax/libs/jqueryui/1.8.0/jquery-ui.min.js"></script>


        <!--[if lt IE 9]>
            <script src="//html5shim.googlecode.com/sin/trunk/html5.js"></script>
        <![endif]-->
        <?php
// %^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^% 
// 
//  javascript code for the register page to allow a person to add group members
//
// CSS Files: Bootstrap and Custom CSS

        print '<link rel="stylesheet" href="' . CSS_PATH . '/bootstrap.min.css">';
        print LINE_BREAK;
        print '<link rel="stylesheet" href="' . CSS_PATH . '/custom.css">';
        print LINE_BREAK;

// Boostrap Modernizer
        print '<script src="' . JS_PATH . '/flexslider/modernizr.js"></script>';
        print LINE_BREAK;
        print '<script src="' . JS_PATH . '/bootstrap.min.js"></script>';
        print LINE_BREAK;
        print '<script src="' . JS_PATH . '/jquery.flexslider.js"></script>';

// Google fonts
        print LINE_BREAK;
        print "<link href='//fonts.googleapis.com/css?family=Yesteryear' rel='stylesheet' type='text/css'>";

        print LINE_BREAK;
        print "<!-- custom.js " . JS_PATH . '/custom.php -->';

        print LINE_BREAK;

        print '</head>';

        print '<body id="' . $PATH_PARTS["filename"] . '">';

        print '<section id="page-wrap">';
        include ("menu-2.php");
        include ("header.php");
        ?>