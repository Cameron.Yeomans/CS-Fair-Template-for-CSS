<?php
include ("top-2.php");
?>
    
<section id="judges">
    <div id="judgingThanks" class="row">
        <h1 class="page-title">Judges</h1>

        <div class="col-md-4">
            <img src="images/judges.png" alt="CS Fair Judges" class="img-thumbnail dis" />
        </div>
        <div class="col-md-8">
            <p class="lead">We would like to thank all of our judges who have donated their time and expertise to be with 
                us at the 2016 CS Fair. Your help is greatly appreciated. Would you like to be a judge at this event? Become a sponsor By: filling out our sponsorship form to get started (its a lot of fun).
            </p>
            <!--       <a href="sponsorsForm.php">
                       <button type="button" class="btn btn-primary btn-lg btn-block">Become a Judge</button>
                   </a> -->
        </div>


        <div class="row judging-criteria">
            <h2>How Judging Will Work</h2>
            <div class="col-md-4">
                <h3>Judging Teams</h3>
                <p>This year each judge will be assigned to a team to judge a particular category. Each team of judges will be expected to look at all the projects for the category they have been assigned to picking the 1<sup>st</sup>, 2<sup>nd</sup>, 3<sup>rd</sup> place winners for their category. Judging teams will be assigned at the judge's orientation on the day of the fair.</p> 
            </div>
            <div class="col-md-4">
                <h3>Random Hourly</h3>
                Prizes are picked each hour via a PHP page that uses MySQL ORDER BY RAND() LIMIT 1. A winner will be selected for each hour block which includes 1:10 pm to 2:00 pm, 2:20 pm to 3:10 pm, 3:30 pm to 4:20 pm.       
                You must be present to claim your prize.
            </div>
        </div> <!-- how judging works -->

        <section class="slider"><div class="flexslider"><ul class="judges list-unstyled slides">
                    <li>
                        <div class="row"><div class="col-md-4"><article class='judge-pic-container' style="background-image:url('images/judges/Joel Bartley pic.png');">
                                </article></div>
                            <div class="col-md-8"><h2>Joel&nbsp;Bartley</h2><h4>Software Engineer from IBM</h4><p>Joel Bartley is a Software Engineer at IBM in Burlington, VT with 20 years of experience with Databases, Java and Web Development. He&#039;s followed the development of multiple programming languages over time and his current interests are focused on parallel programming. </p></div></div></li>

                    <li>
                        <div class="row"><div class="col-md-4"><article class='judge-pic-container' style="background-image:url('images/judges/cohn.jpg');">
                                </article></div>
                            <div class="col-md-8"><h2>John&nbsp;Cohn</h2><h4>IBM Fellow from </h4><p>John Cohn is an IBM Fellow in IBM Corporate Technical Strategy. His current focus is on physical infrastructure for Smarter Cities, open data, machine to machine communications and real time data analytics.</p></div></div></li>

                    <li>
                        <div class="row"><div class="col-md-4"><article class='judge-pic-container' style="background-image:url('images/judges/MichaelCommo.jpg');">
                                </article></div>
                            <div class="col-md-8"><h2>Michael&nbsp;Commo</h2><h4>Senior Software Engineer from Galen Healthcare Solutions</h4><p></p></div></div></li>

                    <li>
                        <div class="row"><div class="col-md-4"><article class='judge-pic-container'>
                                </article></div>
                            <div class="col-md-8"><h2>Julie&nbsp;Daly</h2><h4>Implementation Consultant from Fast Enterprises, LLC</h4><p></p></div></div></li>

                    <li>
                        <div class="row"><div class="col-md-4"><article class='judge-pic-container' style="background-image:url('images/judges/Deering Profile Pic.jpg');">
                                </article></div>
                            <div class="col-md-8"><h2>Peter&nbsp;Deering</h2><h4>Vice President, Application Architecture Team, State Street Corporation from State Street Corporation</h4><p>During his 25 years at State Street, Peter has managed a number of large application development teams that has supported a variety of business areas. His teams have designed, developed, and implemented large-scale accounting and custody applications that supported institutional clients and global investment managers. Peter is currently leading development of a large scale, cloud based application that is a key component in State Street&amp;rsquo;s digitization strategy. He holds a BA from ColBy: College and an MBA specializing in Management Information Systems from Suffolk University.</p></div></div></li>

                    <li>
                        <div class="row"><div class="col-md-4"><article class='judge-pic-container'>
                                </article></div>
                            <div class="col-md-8"><h2>Josh&nbsp;Dickerson</h2><h4>Senior Developer Advertising - Real-Time Bidding from Dealer.com</h4><p></p></div></div></li>

                    <li>
                        <div class="row"><div class="col-md-4"><article class='judge-pic-container' style="background-image:url('images/judges/me.jpg');">
                                </article></div>
                            <div class="col-md-8"><h2>Jim&nbsp;Eddy</h2><h4>Chief Technology Officer from Vermont HITEC</h4><p>Jim Eddy joined Vermont HITEC in 2005 and holds the position of Chief Technology Officer. Jim obtained his B.A. in Physics and Applied Mathematics from the University of Vermont, and holds a M.S. in Information Systems from Northwestern University. Jim has worked with numerous organizations on consulting projects, strategic direction, process improvement and workforce development solutions over the past twelve years. Jim also teaches several courses as a part-time faculty member in the UVM Department of Computer Science. </p></div></div></li>

                    <li>
                        <div class="row"><div class="col-md-4"><article class='judge-pic-container' style="background-image:url('images/judges/Screen Shot 2016-11-18 at 9.08.19 AM.png');">
                                </article></div>
                            <div class="col-md-8"><h2>Ian &nbsp;Foertsch</h2><h4>Software Engineer from NextCapital Group</h4><p></p></div></div></li>

                    <li>
                        <div class="row"><div class="col-md-4"><article class='judge-pic-container' style="background-image:url('images/judges/Ben-Glassman.jpg');">
                                </article></div>
                            <div class="col-md-8"><h2>Ben&nbsp;Glassman</h2><h4>CTO from Vermont Design Works</h4><p>Ben began working with web technology since he first acquired a dial-up Internet connection. His professional career started as a freelance web developer and graphic designer in southern Massachusetts. He moved to Vermont in 2001 to pursue a Bachelor's Degree in Multimedia and Graphic Design from Champlain College. After graduating in 2005, Ben joined the VDW team, bringing his experience in web development and a passion for building accessible, standards compliant web sites. He is a versatile polyglot programmer and full-stack web developer, combining expertise in front-end JavaScript development with experience in server side technologies using PHP and MySQL. Ben serves as technical lead for VDW's most complicated projects, working with clients to help translate their needs into a deliverable solution. He works closely with our design team to prototype functionality and converts visual details into working websites.</p></div></div></li>

                    <li>
                        <div class="row"><div class="col-md-4"><article class='judge-pic-container'>
                                </article></div>
                            <div class="col-md-8"><h2>Tabare&nbsp;Gowon</h2><h4>Java Developer from Dealer.com</h4><p></p></div></div></li>

                    <li>
                        <div class="row"><div class="col-md-4"><article class='judge-pic-container' style="background-image:url('images/judges/SJ.jpg');">
                                </article></div>
                            <div class="col-md-8"><h2>Steven&nbsp;Jenkins</h2><h4>VP North America from QA Consulting</h4><p></p></div></div></li>

                    <li>
                        <div class="row"><div class="col-md-4"><article class='judge-pic-container' style="background-image:url('images/judges/irk_square.jpg');">
                                </article></div>
                            <div class="col-md-8"><h2>Isaiah&nbsp;Keepin</h2><h4>Senior Developer from Bluehouse Group</h4><p>Equal parts storyteller, architect, and entrepreneur, Isaiah is passionate about the vast range of expression and connection that the web makes possible. Ever since the 1980&rsquo;s, when he wrote his first computer game on a Commodore 64, he has loved exploring the intersection of human stories and systems of information. These days, he works mainly in JavaScript, PHP, and MySQL, but you never know where you&rsquo;ll find him next. When he&rsquo;s not weaving code, he loves exploring the woods of Vermont, reading the latest Haruki Murakami novel, and homeschooling his five kids.</p></div></div></li>

                    <li>
                        <div class="row"><div class="col-md-4"><article class='judge-pic-container' style="background-image:url('images/judges/sean kio.jpg');">
                                </article></div>
                            <div class="col-md-8"><h2>Sean&nbsp;Kio</h2><h4>Burlington Telecom Graphic Designer and Webmaster from Burlington Telecom</h4><p>Sean Kio is Operations Director for Kio Creative and Burlington Telecom&amp;#039;s Graphic Designer &amp;amp; Webmaster.  He has worked in the industry of web development and graphic design for more than 5 years.</p></div></div></li>

                    <li>
                        <div class="row"><div class="col-md-4"><article class='judge-pic-container'>
                                </article></div>
                            <div class="col-md-8"><h2>Rama&nbsp;Kocherlakota</h2><h4>Senior Java Developer from Dealer.com</h4><p></p></div></div></li>

                    <li>
                        <div class="row"><div class="col-md-4"><article class='judge-pic-container'>
                                </article></div>
                            <div class="col-md-8"><h2>Luc&nbsp;Martin</h2><h4>Business Development from NuHarbor Security</h4><p></p></div></div></li>

                    <li>
                        <div class="row"><div class="col-md-4"><article class='judge-pic-container' style="background-image:url('images/judges/Tony MateroHS.PNG');">
                                </article></div>
                            <div class="col-md-8"><h2>Tony&nbsp;Matero</h2><h4>Technical Writer from MyWebGrocer</h4><p>Tony spends his time making complex information easier to understand. He&amp;amp;rsquo;s currently a technical writer at MyWebGrocer based in Winooski, VT, a leading ecommerce company that manages digital solutions for many retailers across the globe. Before devoting his time to technical writing, Tony dedicated years helping customers and coworkers use and repair biotechnology equipment. Through his experience, he has developed an ability to understand customers and quickly meet their needs. Tony loves to travel with his wife and daughter, recently visiting Kuala Lumpur, Copenhagen, Seattle, and Alaska. His favorite places include San Francisco, New Zealand, and Burlington, VT.</p></div></div></li>

                    <li>
                        <div class="row"><div class="col-md-4"><article class='judge-pic-container' style="background-image:url('images/judges/EverettMcKay.png');">
                                </article></div>
                            <div class="col-md-8"><h2>Everett&nbsp;McKay</h2><h4>Principal from UX Design Edge</h4><p>Everett McKay is Principal of UX Design Edge, a user experience design training and consulting company for mobile, web, and desktop applications. Everett&#039;s specialty is UX design training for software professionals who aren&#039;t experienced designers through onsite and virtual courses and workshops. He has delivered UX design workshops to an international audience that includes Europe (UK, Ireland, Poland, Greece, Turkey), Asia (India, China), South America (Argentina), and Africa (South Africa, Cameroon). <br />
                                </p></div></div></li>

                    <li>
                        <div class="row"><div class="col-md-4"><article class='judge-pic-container' >
                                </article></div>
                            <div class="col-md-8"><h2>Brendan&nbsp;McOmber</h2><h4>Implementation Consultant from Fast Enterprises, LLC</h4><p></p></div></div></li>

                    <li>
                        <div class="row"><div class="col-md-4"><article class='judge-pic-container' style="background-image:url('images/judges/AndersMelen.png');">
                                </article></div>
                            <div class="col-md-8"><h2>Anders&nbsp;Melen</h2><h4>Mobile Software Engineer from Green Mountain Software Corporation</h4><p>Anders was born and raised in Vermont. He graduated from the University of Vermont with a bachelor&amp;amp;amp;amp;rsquo;s degree in computer science. He specializes in mobile app development for iOS. In his free time, he enjoys skateboarding, cars, motorcycles and whatever this month&amp;amp;amp;amp;rsquo;s hobBy: is.</p></div></div></li>

                    <li>
                        <div class="row"><div class="col-md-4"><article class='judge-pic-container' style="background-image:url('images/judges/JohnNeed.jpg');">
                                </article></div>
                            <div class="col-md-8"><h2>John &nbsp;Need</h2><h4>Senior Software Engineer from Galen Healthcare Solutions</h4><p></p></div></div></li>

                    <li>
                        <div class="row"><div class="col-md-4"><article class='judge-pic-container'>
                                </article></div>
                            <div class="col-md-8"><h2>Eric&nbsp;Newbury</h2><h4>Java Programmer from Dealer.com</h4><p></p></div></div></li>

                    <li>
                        <div class="row"><div class="col-md-4"><article class='judge-pic-container' style="background-image:url('images/judges/Melissa.jpg');">
                                </article></div>
                            <div class="col-md-8"><h2>Melissa&nbsp;Odom</h2><h4>Software Manager from BAE Systems</h4><p>Melissa woks in the Intelligence, Surveillance &amp; Reconnaissance Solutions Business Area at BAE systems as the software manager for the SOCOM Programs.  She is also the software college and intern hiring manager for all of southern New Hampshire.  Melissa holds a BS in Computer Science from the University of Mary Washington.</p></div></div></li>

                    <li>
                        <div class="row"><div class="col-md-4"><article class='judge-pic-container' style="background-image:url('images/judges/Jeremy Patrie.jpg');">
                                </article></div>
                            <div class="col-md-8"><h2>Jeremy&nbsp;Patrie</h2><h4>Burlington Telecom Sr Division Manager of Network Operations Engineering and Service Support from Burlington Telecom</h4><p>Jeremy Patrie is Burlington Telecom&amp;#039;s Senior Division Manager of Network Operations, Engineering and Service Support and has worked in the telecommunications industry for over 14 years.</p></div></div></li>

                    <li>
                        <div class="row"><div class="col-md-4"><article class='judge-pic-container' style="background-image:url('images/judges/Screen Shot 2016-09-20 at 1.40.23 PM.png');">
                                </article></div>
                            <div class="col-md-8"><h2>Lauren &nbsp;Petrie</h2><h4>Coordinator, CEMS Career Readiness Program from CEMS Career Readiness Program</h4><p></p></div></div></li>

                    <li>
                        <div class="row"><div class="col-md-4"><article class='judge-pic-container' style="background-image:url('images/judges/DSC_0649.jpg');">
                                </article></div>
                            <div class="col-md-8"><h2>Scott&nbsp;Pfeiffer</h2><h4>Principal Process Engineer from GLOBALFOUNDRIES</h4><p>Graduate of Penn State 2012<br />
                                    Worked at IBM from 2012 to 2015<br />
                                    Worked at Globalfoundries from 2015 to current<br />
                                </p></div></div></li>

                    <li>
                        <div class="row"><div class="col-md-4"><article class='judge-pic-container' style="background-image:url('images/judges/Zp.jpg');">
                                </article></div>
                            <div class="col-md-8"><h2>Zac&nbsp;Porter</h2><h4>Resource Operations Manager from QA Consulting</h4><p></p></div></div></li>

                    <li>
                        <div class="row"><div class="col-md-4"><article class='judge-pic-container' style="background-image:url('images/judges/Profile Updated crop2.jpg');">
                                </article></div>
                            <div class="col-md-8"><h2>Anders&nbsp;Reinertsen</h2><h4>Vice President, Strategic Integration, Technology from State Street Corporation</h4><p>As part of the Strategic Integration team, he is currently focusing on ensuring our enterprise software development portfolio achieves the intended business outcomes and aligns to our end state technology vision. He works with the agile delivery teams to ensure they deliver reusable components and understand the end to end impact of their software. We partner with business leads to create new initiatives based on what is possible instead of what exists today.  Anders also leads the application rationalization program across the enterprise.</p></div></div></li>

                    <li>
                        <div class="row"><div class="col-md-4"><article class='judge-pic-container'>
                                </article></div>
                            <div class="col-md-8"><h2>Samuel&nbsp;Reinhardt</h2><h4>Commodore of Interactive Development from Brandthropology</h4><p></p></div></div></li>

                    <li>
                        <div class="row"><div class="col-md-4"><article class='judge-pic-container'>
                                </article></div>
                            <div class="col-md-8"><h2>Jon&nbsp;Ricketson</h2><h4>Senior Vice President from State Street Corporation</h4><p>Jon Ricketson is a Senior Vice President within the ADM organization at State Street.  He has responsibility for all software development and application support activities for State Street&amp;rsquo;s Accounting, Custody, Cash and Reconciliations platforms. Jon is also application development Delivery Manager within the State Street Beacon program.<br />
                                    <br />
                                    Jon joined State Street in 1987 as a Systems Officer in Information Technology and has managed several large mainframe and open systems development efforts.  From 1999 to 2005, Jon was head of Management Information Services, where he had responsibility for State Street&amp;#039;s Corporate software applications including all financial systems, human resources management, compensation and payroll systems, marketing and sales support, information security administration, strategic sourcing, risk management and legal. From 1996 until 1999, Jon was the Director of European Technology in London where he was responsible for planning, coordinating and executing the rollout of core software applications as well as implementing a number of European client-specific initiatives.  <br />
                                    <br />
                                    Prior to joining State Street, Jon was a Senior Consultant with Computer Partners.  Jon holds a BS in Mathematics and Statistics from the University of Massachusetts and an MBA summa cum laude in Management Science from the University of Rhode Island.<br />
                                </p></div></div></li>

                    <li>
                        <div class="row"><div class="col-md-4"><article class='judge-pic-container' style="background-image:url('images/judges/AAEAAQAAAAAAAAI6AAAAJGQyZDc0NTFiLTc1MmMtNDgwNy1hYzNlLWNiOTM4YjM1NmE2OQ.jpg');">
                                </article></div>
                            <div class="col-md-8"><h2>Greg &nbsp;Roughton</h2><h4>Software Developer from Logic Supply</h4><p>Greg is a Software Engineer working on web development at Logic Supply. His development practices rely heavily on internal and external stakeholder/customer feedback. Outside of work he&#039;s an avid woodworker and home renovator. His current big project is rejuvenating our 1850s era house while maintaining it&#039;s hard earned quirks.</p></div></div></li>

                    <li>
                        <div class="row"><div class="col-md-4"><article class='judge-pic-container' style="background-image:url('images/judges/Kyle Sarrazin pic.jpg');">
                                </article></div>
                            <div class="col-md-8"><h2>Kyle&nbsp;Sarrazin</h2><h4>Senior Software Engineer from IBM</h4><p>Kyle Sarrazin is a Senior Software Engineer at IBM&#039;s Systems Division in Burlington, VT.  His current focus is on enabling data analytics across IBM Systems&#039; product suite.</p></div></div></li>

                    <li>
                        <div class="row"><div class="col-md-4"><article class='judge-pic-container' style="background-image:url('images/judges/image1.JPG');">
                                </article></div>
                            <div class="col-md-8"><h2>Ian&nbsp;Scott</h2><h4>Senior Software Engineer from C2</h4><p>Ian Scott is a Senior Software Engineer with C2. He provides custom web-based solutions to C2&rsquo;s local and national clients. Ian brings over 16 years of software development expertise working for companies such as Sony, Stonehouse Media and Comcast.</p></div></div></li>

                    <li>
                        <div class="row"><div class="col-md-4"><article class='judge-pic-container' style="background-image:url('images/judges/bennett.png');">
                                </article></div>
                            <div class="col-md-8"><h2>Bennett&nbsp;Siegel</h2><h4>Software Engineer from Green Mountain Software Corporation</h4><p>Bennett is a Michigan native who&amp;amp;amp;amp;rsquo;s called Vermont home since 2010. After graduating from the University of Vermont with a degree in computer science, he started working as a software engineer for Green Mountain Software. His specialties include data collection, processing and web services. In his free time, he enjoys skiing and cooking barbecue.</p></div></div></li>

                    <li>
                        <div class="row"><div class="col-md-4"><article class='judge-pic-container' style="background-image:url('images/judges/guillaume.jpg');">
                                </article></div>
                            <div class="col-md-8"><h2>Guillaume&nbsp;Sparrow-Pepin</h2><h4>Teacher and System Administrator from </h4><p>Guillaume is the system administrator and teaches programming at The Putney School. He graduated from the University of Vermont in 2015 with a degree in Computer Science and a minor in Music Performance. He has worked for many years as a freelance web developer. Guillaume participated in the CS Fair as an undergraduate and judged the 2015 CS fair.</p></div></div></li>

                    <li>
                        <div class="row"><div class="col-md-4"><article class='judge-pic-container'>
                                </article></div>
                            <div class="col-md-8"><h2>Danielle&nbsp;Steimke</h2><h4>UI Developer from Dealer.com</h4><p></p></div></div></li>

                    <li>
                        <div class="row"><div class="col-md-4"><article class='judge-pic-container' style="background-image:url('images/judges/MichaelTamlyn.jpg');">
                                </article></div>
                            <div class="col-md-8"><h2>Michael&nbsp;Tamlyn</h2><h4>Principal Software Architect from Galen Healthcare Solutions</h4><p></p></div></div></li>

                    <li>
                        <div class="row"><div class="col-md-4"><article class='judge-pic-container' style="background-image:url('images/judges/Katie Taylor Photo.jpg');">
                                </article></div>
                            <div class="col-md-8"><h2>Katie&nbsp;Taylor</h2><h4>Director of Workforce Development and Entrepreneurship from HackVT</h4><p>Katie runs the entrepreneurial and young professional programs at the Lake Champlain Regional Chamber of Commerce, including LaunchVT, HackVT, and Burlington Young Professionals. Prior to joining the Chamber, Katie worked as an environmental attorney, most recently with the Compliance &amp;amp; Enforcement Division of the Vermont Department of Environmental Conservation. Katie also worked at Crowell &amp;amp; Moring in Washington, D.C. Katie received her Juris Doctor from the George Washington University Law School, a Masters of Fine Arts in theatre from the Catholic University of America in Washington, D.C., and a Bachelor of Arts in English from Wesleyan College in Macon, GA.</p></div></div></li>

                    <li>
                        <div class="row"><div class="col-md-4"><article class='judge-pic-container' style="background-image:url('images/judges/Terry Ulmer pic.jpg');">
                                </article></div>
                            <div class="col-md-8"><h2>Terry&nbsp;Ulmer</h2><h4>Software Engineer from IBM</h4><p>Terry Ulmer is UVM alumnus and a Software Engineer at IBM in Burlington, VT. He is currently focused on providing advanced analytics and reporting to IBM server and storage development teams.</p></div></div></li>

                    <li>
                        <div class="row"><div class="col-md-4"><article class='judge-pic-container' style="background-image:url('images/judges/colin.png');">
                                </article></div>
                            <div class="col-md-8"><h2>Colin&nbsp;Urban</h2><h4>Web Developer from Bluehouse Group</h4><p>If the future of our species lies in the creative harmony of wilderness and technology, then Colin is a man from the future, crafting cutting-edge code while homesteading off the grid among the foothills of Camel&#039;s Hump. He is passionate about building or fixing anything, bringing an eye for beauty and utility whether it&#039;s a PHP function, a tele-presence robot, or the latest improvement on the yurt he lives in with his wife, dogs and cat.<br />
                                    <br />
                                    Colin has been programming in one form or another for a decade and a half, and loves to keep abreast with the latest ideas in the field. Along with coding, Colin&#039;s interests include news and current events, underground hip hop and spending time in nature.</p></div></div></li>

                    <li>
                        <div class="row"><div class="col-md-4"><article class='judge-pic-container' style="background-image:url('images/judges/183521e.jpg');">
                                </article></div>
                            <div class="col-md-8"><h2>Neil&nbsp;Wacek</h2><h4>Online Communication and Agency Interface Specialist  from Union Mutual</h4><p></p></div></div></li>

                    <li>
                        <div class="row"><div class="col-md-4"><article class='judge-pic-container' style="background-image:url('images/judges/MattWard.jpg');">
                                </article></div>
                            <div class="col-md-8"><h2>Matt&nbsp;Ward</h2><h4>CTO from Green Mountain Software Corporation</h4><p>Matt Ward is the CTO of Green Mountain Software. During his time at Vermont Technical College in the Software Engineering program, he was the president of the hockey club, named Technician of the Year, and served as a key researcher/developer for the first-ever CubeSat program using SPARK and Ada programming.</p></div></div></li>

                    <li>
                        <div class="row"><div class="col-md-4"><article class='judge-pic-container' style="background-image:url('images/judges/christie.jpg');">
                                </article></div>
                            <div class="col-md-8"><h2>Christie&nbsp;Woodward</h2><h4>Lead Full Stack Developer from NextCapital Group</h4><p></p></div></div></li>

                </ul>   </div></section>
    </div> <!-- judgingThanks-->
</section> <!-- judges --> 

<?php include ("footer.php"); ?>
</section> <!-- page-wrap in top.php-->
</body>
</html>

