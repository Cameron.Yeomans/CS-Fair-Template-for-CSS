<?php

class myProject {

// print "<p>SQL: " . $sql . "<p><pre>"; print_r($data); print "</pre></p>";
// $this->db->select select($query, $values = "", $whereAllowed = true, $conditions = 0)
    var $db;

    public function __construct($db) {
        $this->db = $db;
    }

//############################################################################
    function allProjects($category = 0, $sortOrder = "fldTime") {
        $sql = "SELECT pmkProjectId, fldQrCode, fldProjectName, fldProjectSite, ";
        $sql .= "fldProjectDesc, fldProjectCourses, fldProjectImageURL, ";
        $sql .= "fldProjectBoothNum,fldBoothSide, fldPresentationTimeSlot, fldCategory, fldColorCode, fldTime, fldConfirmed, tblProject.fldApproved ";
        $sql .= "FROM tblProject JOIN tblCategory ON pmkCategoryId = fnkCategoryId ";
        $sql .= "JOIN tblTimeSlot ON pkTimeSlotId=fldPresentationTimeSlot  ";
        $sql .= "WHERE tblProject.fnkCategoryId = " . $category . " ";
        $sql .= "ORDER BY " . $sortOrder;

        $results = $this->db->select($sql);
        if ($results) {
            return $results;
        }
        return "";
    }

//############################################################################
    function allApprovedProjects($sortOrder = "pmkProjectID", $category = 0) {

        $sql = "SELECT pmkProjectId, fldQrCode, fldProjectName, fldProjectSite, ";
        $sql .= "fldProjectDesc, fldProjectCourses, fldProjectImageURL, ";
        $sql .= "fldProjectBoothNum,fldBoothSide, fldPresentationTimeSlot, fldCategory, fldColorCode, fldTime ";
        $sql .= "FROM tblProject JOIN tblCategory ON pmkCategoryId = fnkCategoryId ";
        $sql .= "JOIN tblTimeSlot ON pkTimeSlotId=fldPresentationTimeSlot  ";
        $sql .= "WHERE tblProject.fldApproved>0 ";
        if ($category != 0) {
            $sql .= "AND tblProject.fnkCategoryId = ? ";
            $data = array((int) $category);
        }
        $sql .= "ORDER BY " . $sortOrder;

        if ($category == 0) {
            $results = $this->db->select($sql);
        } else {
            $results = $this->db->select($sql, $data);
        }

        if ($results) {
            return $results;
        }
        return "";
    }

    function totalStudents() {

        $sql = "SELECT pkUsername ";
        $sql .= "FROM tblStudent ";
        $sql .= "JOIN tblStudentProject ON pkUsername = fkUVMId ";
        $sql .= "JOIN tblProject ON 	pmkProjectId = fkProjectId ";
        $sql .= "WHERE fldApproved > 0 ";
        $sql .= "ORDER BY pkUsername ASC";

        $results = $this->db->select($sql);

        if ($results) {
            return $results;
        }
        return "";
    }

//############################################################################
    function projectsForStudent($sid) {
        if (empty($sid))
            return "";

        $output = "";

        $sql = "SELECT distinct fkProjectId, fldProjectName ";
        $sql .= "FROM tblStudentProject ";
        $sql .= "JOIN tblProject ON pmkProjectId = fkProjectId ";
        $sql .= "WHERE fkUVMId = ? ";
        $sql .= "ORDER BY fldProjectName";

        $data = array($sid);

        $results = $this->db->select($sql, $data);

        if (!empty($results)) {
            return $results;
        }
        return "";
    }

    //############################################################################
    function project($pid) {
        if (empty($pid))
            return "";

        $sql = "SELECT pmkProjectId, fldProjectName, fldProjectSite, ";
        $sql .= "fldProjectDesc, fldProjectCourses, fldProjectImageURL, ";
        $sql .= "fldPresentationTimeSlot, fnkCategoryId, fldProjectBoothNum, fldBoothSide ";
        $sql .= "FROM tblProject ";
        $sql .= "WHERE pmkProjectId= ?";

        $data = array($pid);

        $results = $this->db->select($sql, $data);

        if ($results) {
            return $results;
        }
        return "";
    }

//############################################################################
    function listProjectsForStudent($sid, $plus) {
        $projects = $this->projectsForStudent($sid);

        if (!$projects)
            return "";

        $output = array();
        $output[] = "<ol class='studentProjects'>";
        foreach ($projects as $project) {
            $output[] = '<li><a href="form.php?pid=' . $project["fkProjectId"] . '">' . $project["fldProjectName"] . '</a></li>';
        }

        if (count($this->projectsForStudent($sid)) < 3) {
            if ($plus) {
                $output[] = '<li><a href="form.php?pid=0">Enter New Project</a></li>';
            }
        }
        $output[] = "</ol>";

        return join("\n", $output);
    }

//############################################################################
    function totalProjectMembers($pid) {
        if (empty($pid))
            return "";

        $output = "";

        $sql = "SELECT count(fldFirstName) as totalMembers ";
        $sql .= "FROM tblStudent, tblStudentProject ";
        $sql .= "WHERE pkUsername = fkUVMId ";
        $sql .= "AND fkProjectId = ?";

        $data = array($pid);

        $results = $this->db->select($sql, $data, true, 1);

        return $results[0]['totalMembers'];
    }

//############################################################################
    function projectMember($pid, $user) {
        if (empty($pid))
            return "";
        if (empty($user))
            return "";

        $sql = "SELECT fkUVMId ";
        $sql .= "FROM tblStudentProject ";
        $sql .= "WHERE fkProjectId = ? ";
        $sql .= "AND fkUVMId = ? ";

        $data = array((int) $pid, $user);

        $results = $this->db->select($sql, $data, true, 1);

        return $results;
    }

    //############################################################################
    function projectLeader($pid) {
        if (empty($pid))
            return "";

        $sql = "SELECT fkUVMId ";
        $sql .= "FROM tblStudentProject ";
        $sql .= "WHERE fkProjectId = ? ";
        $sql .= "AND fldOrder = 1";

        $data = array((int) $pid);

        $results = $this->db->select($sql, $data, true, 1);

        return $results[0]["fkUVMId"];
    }

    //############################################################################
    function projectLeaderName($pid) {
        if (empty($pid))
            return "";

        $output = "";

        $sql = "SELECT CONCAT_WS(' ', fldFirstName, fldLastName) as fldLeaderName ";
        $sql .= "FROM tblStudent, tblStudentProject ";
        $sql .= "WHERE pkUsername = fkUVMId ";
        $sql .= "AND fkProjectId = ? ";
        $sql .= "AND fldOrder = 1 ";

        $data = array((int) $pid);

        $results = $this->db->select($sql, $data, true, 2);
        if ($results) {
            return $results[0]["fldLeaderName"];
        }
        return "";
    }

    //############################################################################
    function projectTeamMembers($pid) {
        if (empty($pid))
            return "";

        $output = "";

        $sql = "SELECT pkUsername, fldFirstName, fldLastName, fldTshirtSize ";
        $sql .= "FROM tblStudent, tblStudentProject ";
        $sql .= "WHERE pkUsername = fkUVMId ";
        $sql .= "AND fkProjectId = ? ";
        $sql .= "ORDER BY fldOrder";

        $data = array((int) $pid);
        $results = $this->db->select($sql, $data, true, 2);

        $output = "<span class='studentPresentor'>";
        $comma = 0;
        foreach ($results as $member) {
            if ($comma >= 1)
                $output .= ", ";
            $comma++;
            $output .= $member["fldFirstName"] . "&nbsp;" . $member["fldLastName"];
        }
        $output .= "</span> ";
        return $output;
    }

//############################################################################
    function projectMembers($pid) {
        if (empty($pid))
            return "";

        $output = "";

        $sql = "SELECT fldFirstName, fldLastName ";
        $sql .= "FROM tblStudent, tblStudentProject ";
        $sql .= "WHERE pkUsername = fkUVMId ";
        $sql .= "AND fkProjectId = ? ";
        $sql .= "ORDER BY fldOrder";

        $data = array((int) $pid);

        $results = $this->db->select($sql, $data, true, 2);

        if ($results) {
            return $results;
        }
        return "";
    }

//############################################################################
    function projectName($pid) {
        if (empty($pid))
            return "";

        $sql = "SELECT fldProjectName ";
        $sql .= "FROM tblProject ";
        $sql .= "WHERE pmkProjectId = ?";

        $data = array((int) $pid);

        $results = $this->db->select($sql, $data);

        if (isset($results[0]['fldProjectName'])) {

            return $results[0]['fldProjectName'];
        } else {
            return "";
        }
    }

    //############################################################################
    function removeStudents($pid) {
        if (empty($pid))
            return "";
        $sql = "DELETE FROM tblStudentProject WHERE fkProjectId = ?";
        $data = array($pid);
        return $this->db->delete($sql, $data);
    }

//############################################################################
    function sortORder($int) {
        if (empty($int))
            return "";
        switch ($int) {
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
                $sortOrder = "fldPresentationTimeSlot, pmkProjectId";
                break;

            case 10:
                $sortOrder = "fldPresentationTimeSlot, fldProjectDesc";
                break;

            case 20:
                $sortOrder = "fldProjectBoothNum, fldBoothSide, fldPresentationTimeSlot";
                break;

            case 30:
                $sortOrder = "fldPresentationTimeSlot, fldProjectBoothNum, fldBoothSide ASC";
                break;

            case 40:
                $sortOrder = "fldProjectName, fldDateSubmitted ASC";
                break;

            case 50:
                $sortOrder = "fldDisplayOrder, fldDateSubmitted ASC";
                break;

            default:
                $sortOrder = "RAND()";
        }
        return $sortOrder;
    }

//############################################################################
    function sortDisplay($int) {
        if (empty($int))
            return "";
        switch ($int) {
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
                $sortDisplay = "Sorted By Presentation Time and Project Id";
                break;

            case 10:
                $sortDisplay = "Sorted by Presentation Time";
                break;

            case 20:
                $sortDisplay = "Sorted by Booth Number";
                break;

            case 30:
                $sortDisplay = "Sorted by Presentation Time";
                break;

            case 40:
                $sortDisplay = "Sorted by Project Name";
                break;
            case 50:

                $sortDisplay = "Sorted by Project Name";
                break;
            default:
                $sortDisplay = "Random Sort";
        }
        return $sortDisplay;
    }

//############################################################################
    function projectTableNumber($pid) {
        if (empty($pid))
            return "";

        $sql = "SELECT fldProjectBoothNum, fldBoothSide ";
        $sql .= "FROM tblProject ";
        $sql .= "WHERE pmkProjectId = ?";

        $data = array((int) $pid);

        $results = $this->db->select($sql, $data);

        if (isset($results[0]['fldProjectBoothNum'])) {
            return $results[0]['fldProjectBoothNum'] . " - " . $results[0]['fldBoothSide'];
        } else {
            return "";
        }
    }

//############################################################################
    function projectTime($pid) {
        if (empty($pid))
            return "TBD";

        $sql = "SELECT fldPresentationTimeSlot ";
        $sql .= "FROM tblProject ";
        $sql .= "WHERE pmkProjectId = ?";

        $data = array((int) $pid);

        $results = $this->db->select($sql, $data);

        if (isset($results[0]["fldPresentationTimeSlot"])) {
            $sql = "SELECT fldTime ";
            $sql .= "FROM tblTimeSlot ";
            $sql .= "WHERE pkTimeSlotId = ?";

            $T_data = array($results[0]['fldPresentationTimeSlot']);

            $results = $this->db->select($sql, $T_data);

            if (isset($results[0]['fldTime'])) {
                return $results[0]['fldTime'];
            }
            return "Time TBD";
        }
        return "TBD";
    }

//############################################################################
// returns total numer of projects for this category
    function allCategories() {

        $sql = "SELECT pmkCategoryId, ";
        $sql .= "fldOneWinnerProjectId, fldTwoWinnerProjectId, fldThreeWinnerProjectId ";
        $sql .= "FROM tblCategory ";
        $sql .= "WHERE fldApproved = 1 ";
        $sql .= "ORDER BY fldDisplayOrder ASC";

        $results = $this->db->select($sql);

        return $results;
    }

//############################################################################
// returns total numer of projects for this category
    function category($cid) {
        if (empty($cid))
            return "";

        $sql = "SELECT fldCategory ";
        $sql .= "FROM tblCategory ";
        $sql .= "WHERE pmkCategoryId = ? ";
        $data = array((int) $cid);
        $results = $this->db->select($sql, $data);

        return $results[0]["fldCategory"];
    }

    //############################################################################
// returns total numer of projects for this category
    function categoryLegend($page="",$categoryChosenId=0) {
        $page = ""; // dont think i need anymore
        $sql = "SELECT 	pmkCategoryId, fldCategory, fldColorCode ";
        $sql .= "FROM tblCategory ";
        $sql .= "WHERE fldApproved >= 1 ";
        $results = $this->db->select($sql);

        $output = "<table id='categoryLegend'>";
        foreach ($results as $row) {
            $output .= '<tr ';
            if($categoryChosenId>0){
                if($row['pmkCategoryId']==$categoryChosenId){
                $output .= ' class="highlight" ';
                }
            }
            $output .= '>';
            $output .= "<th style='background-color: " . $row["fldColorCode"] . "; border: thin solid black;'> &nbsp;</th>";
            $output .= "<td><a href='?cid=" . $row["pmkCategoryId"] . "'>" . $row["fldCategory"] . "</a></td>";
            $output .= "</tr>";
        }
        $output .= "</table>";

        return $output;
    }

//############################################################################
// returns total numer of projects for this category
    function projectsInCategory($cid) {
        if (empty($cid))
            return "";

        $sql = "SELECT pmkProjectId ";
        $sql .= "FROM tblProject ";
        $sql .= "JOIN tblCategory ";
        $sql .= "ON pmkCategoryId = fnkCategoryId ";
        $sql .= "WHERE fnkCategoryId = ? ";
        $sql .= "AND tblProject.fldApproved = 1 ";

        $data = array((int) $cid);

        $results = $this->db->select($sql, $data, true, 1);

        return $results;
    }

//############################################################################
// returns total numer of projects for this category
    function totalProjectsInCategory($cid) {
        if (empty($cid))
            return "";

        $sql = "SELECT count(*) as totalProjects ";
        $sql .= "FROM tblProject ";
        $sql .= "JOIN tblCategory ";
        $sql .= "ON pmkCategoryId = fnkCategoryId ";
        $sql .= "WHERE fnkCategoryId = ? ";
        $sql .= "AND tblProject.fldApproved = 1 ";

        $data = array((int) $cid);
        $results = $this->db->select($sql, $data, true, 1);
        if ($results > 0) {
            return $results[0]['totalProjects'];
        } else {
            return "";
        }
    }

//############################################################################
// returns total numer of projects for this category
    function totalStudentsInCategory($pid) {
        if (empty($pid))
            return "";

        $results = $this->projectsInCategory((int) $pid);

        $totalStudents = 0;
        $t = 0;
        if ($results) {

            foreach ($results as $row) {
                $t = (int) $this->totalProjectMembers($row["pmkProjectId"]);
                $totalStudents = $totalStudents + $t;
            }
        }

        return $totalStudents;
    }

}

// end class
?>
