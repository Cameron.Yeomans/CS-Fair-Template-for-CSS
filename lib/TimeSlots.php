<?php

class TimeSlots {

// $this->db->select select($query, $values = "", $whereAllowed = true, $conditions = 0) 
    var $db;

    public function __construct($db) {
        $this->db = $db;
    }

    //############################################################################
    function availableTimeSlots() {
        $sql = "SELECT pkTimeSlotId, fldTime, fldDisplayOrder, fldNumberOfTables ";
        $sql .= "FROM tblTimeSlot ";
        $sql .= "ORDER BY fldDisplayOrder ";

        $results = $this->db->select($sql, "", false, 0);

        return $results;
    }

//############################################################################
    function timeSlot($tid) {
        $sql = "SELECT fldTime ";
        $sql .= "FROM tblTimeSlot ";
        $sql .= "WHERE pkTimeSlotId = ?";

        $data = array($tid);

        $results = $this->db->select($sql, $data);

        if ($results > 0) {
            return $results[0]['fldTime'];
        }
        return "Time TBD";
    }

}

// end class
?>