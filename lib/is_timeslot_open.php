<?php
//include "lib/constants.php";
// Establish Database Connection
// bin path does not see to work with include so lets just try the code here
$PHP_SELF = htmlentities($_SERVER['PHP_SELF'], ENT_QUOTES, "UTF-8");
$www_rootPath="";
for($i=1; $i<substr_count($PHP_SELF, '/'); $i++){
    $www_rootPath .= "../";
}

define("WEB_ROOT_PATH", $www_rootPath);

// generally I put my passwords outside of the www-root folder so it is not
// in a public folder at all. The web server can access it so still don't
// print your passwords with php code
define("BIN_PATH", $www_rootPath . "../bin");

include(BIN_PATH . '/myDatabase.php');
            
$dbUserName = 'csfair_reader';
$whichPass = "r"; //flag for which one to use.
$dbName = "CSFAIR_2016";

$db = new myDatabase($dbUserName, $whichPass, $dbName);

// Process Inputs from _GET
$PROJECT_CATEGORY = -1;
if (!array_key_exists('project_category', $_GET) ||
        filter_var($_GET['project_category'], FILTER_VALIDATE_INT) === false) {
    die("project_category not an int");
} else {
    $PROJECT_CATEGORY = intval($_GET['project_category']);
}

$GROUP_SIZE = -1;
if (!array_key_exists('group_size', $_GET) ||
        filter_var($_GET['group_size'], FILTER_VALIDATE_INT) === false) {
    die("group_size not an int");
} else {
    $GROUP_SIZE = intval($_GET['group_size']);
}

// Fetch Data
$sql_timeslots = "SELECT pkTimeSlotId, fldTime, fldNumberOfTables FROM tblTimeSlot";
$timeslot_results = $db->select($sql_timeslots, "", false, 0);

$sql_categories = "SELECT pmkCategoryId, fldCategory, fldMaxEntries FROM tblCategory ";
$sql_categories .="WHERE fldApproved != 0";
$category_results = $db->select($sql_categories, "", true, 1);

$sql_projects = "SELECT pmkProjectId, fldGroupSize, fldProjectBoothNum, fldBoothSide, fldPresentationTimeSlot FROM tblProject";
$project_results = $db->select($sql_projects, "", false, 0);

$sql_slots = "SELECT fldPresentationTimeSlot, fnkCategoryId, count(*) AS projects FROM tblProject GROUP BY fldPresentationTImeSlot, fnkCategoryId";
$slot_results = $db->select($sql_slots, "", false, 0);


//print "<!-------------------------------------------------------------------- ->";
//print "<p>times<pre>";print_r($timeslot_results);print"</pre></p>";
//print "<p>categories<pre>";print_r($category_results);print"</pre></p>";
//print "<p>projects<pre>";print_r($project_results);print"</pre></p>";
//print "<p>slot<pre>";print_r($slot_results);print"</pre></p>";
// Prepare Timeslot Stats
$timeslots = array();
foreach ($timeslot_results as $key => $row) {
    $timeslots[$row['pkTimeSlotId']] = array('str' => $row['fldTime'],
        'tables_l' => array_fill(1, $row['fldNumberOfTables'], 0),
        'tables_r' => array_fill(1, $row['fldNumberOfTables'], 0),
        'tables' => array_fill(1, $row['fldNumberOfTables'], 0),
        'available_table' => array(),
        'available_side' => array(),
        'categories' => array());
}

// Then populate each timeslot with the category information
foreach ($category_results as $key => $row) {
    foreach ($timeslots as $timeslot => $row2) {
        $timeslots[$timeslot]['categories'][$row['pmkCategoryId']] = array('str' => $row['fldCategory'],
            'max_entries' => $row['fldMaxEntries'],
            'entries' => 0);
    }
}

// Then go through each project and note where they are positioned
foreach ($project_results as $key => $row) {
    $timeslot = $row['fldPresentationTimeSlot'];
    $group_size = $row['fldGroupSize'];
    $booth = $row['fldProjectBoothNum'];
    $side = $row['fldBoothSide'];

    if ($side == 'north')
        $timeslots[$timeslot]['tables_l'][$booth] = 1;
    else
        $timeslots[$timeslot]['tables_r'][$booth] = 1;
    $timeslots[$timeslot]['tables'][$booth] += $group_size;
}

// Then make sure that for each timeslot and category, we have not reached the max
foreach ($slot_results as $key => $row) {
    $timeslot = $row['fldPresentationTimeSlot'];
    $category = $row['fnkCategoryId'];
    $num = $row['projects'];
    $timeslots[$timeslot]['categories'][$category]['entries'] += $num;
}

//Timeslots can be eliminated based on the following constraints:
foreach ($timeslots as $timeslot => $row) {
    // Check that this project can submit more projects to this timeslot
    if ($timeslots[$timeslot]['categories'][$PROJECT_CATEGORY]['entries'] <
            $timeslots[$timeslot]['categories'][$PROJECT_CATEGORY]['max_entries']) {
        // Check for open tables. Groups of size 3 take up a whole table
        foreach ($timeslots[$timeslot]['tables'] as $table => $students) {
            // Big groups can't share a table
            if ($students >= 3)
                continue;
            if ($GROUP_SIZE >= 3 && ($timeslots[$timeslot]['tables_l'][$table] != 0 ||
                    $timeslots[$timeslot]['tables_r'][$table] != 0))
                continue;

            // Tables can 10+0 ... 4+0 3+0 2+1 2+0 1+1 1+0 0+0
            if ($students == 2 && $GROUP_SIZE >= 2)
                continue;

            if ($timeslots[$timeslot]['tables_l'][$table] == 0) {
                $timeslots[$timeslot]['available_table'][] = $table;
                $timeslots[$timeslot]['available_side'][] = 1;
            }
            if ($timeslots[$timeslot]['tables_r'][$table] == 0) {
                $timeslots[$timeslot]['available_table'][] = $table;
                $timeslots[$timeslot]['available_side'][] = 2;
            }
        }
    }
}

// Print the randomly selected table/side for each timeslot
foreach ($timeslots as $timeslot => $row) {
    if (count($timeslots[$timeslot]['available_table']) == 0)
        continue;
    $position = rand(0, count($timeslots[$timeslot]['available_table']) - 1);

    print $timeslot . "," . $timeslots[$timeslot]['str'] . "," .
            $timeslots[$timeslot]['available_table'][$position] . "," .
            ($timeslots[$timeslot]['available_side'][$position] == 1 ? 'north' : 'south') . ";\n";
}
?>
