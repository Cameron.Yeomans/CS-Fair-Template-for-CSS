<?php
include ("top-2.php");
?>
<section id="main">

    <div class="row intro-row">
        <div class="col-md-4">
            <img src="images/fair.png" alt="CS Fair" class="img-thumbnail dis" />
        </div>
        <div class="col-md-8">

            <p class="lead">
                The CS Fair is a UVM event which provides Computer 
                Science students the opportunity to display web, app, and other computer 
                related projects for the chance to win up to $300 in cash<sup>*</sup>. This year the 
                CS Fair will be held December 9<sup>th</sup> in the Davis Center's Grand Maple Ballroom.
            </p>
            <!--     
               <a class="btn btn-primary btn-lg btn-block" href="register/form.php">
                          Submit a Project!
                      
                      </a>
            -->
            <p><span class="smallText block">Total Projects: 171 Total Students: 286</span></p>        </div>
    </div>
    <!-- 
        <hr>
        <div class="col-md-4 stats dis">
            
    <p class="lead">Registration is now open till! Register By:  November 12, 2016 to receive your free t-shirt.</p>
    <a class="btn btn-primary btn-lg btn-block" href="register/form.php">Submit a Project!</a>
    
    <p><span class="smallText block">We have not closed it yet so enter asap</span></p>
    
      </div>
    -->
    <div class="row prize">


        <section class="col-md-4"><h3>Research Projects</h3><h4>1<sup>st</sup> Place</h4><p class="winner"><span class="project">Emotional arc generator</span> By: <span class='studentPresentor'>Andrew&nbsp;Reagan</span> </p>

        </section>

        <section class="col-md-4"><h3>Advanced Projects<span class="smallText block"> (33 projects, 72 students)</span></h3><h4>1<sup>st</sup> Place $300</h4><p class="winner"><span class="project">IV Drug Compatibility Application for Veterinary Professionals</span> By: <span class='studentPresentor'>Kelly&nbsp;Gray</span> </p><h5>2<sup>rd</sup> Place $200</h5><p class="winner"><span class="project">Allergy Assistant App</span> By: <span class='studentPresentor'>Madison&nbsp;Palmer, Scott&nbsp;Quisenberry, Andrew&nbsp;Green, Ben&nbsp;Stearman, James&nbsp;McCracken, Preston&nbsp;Libby</span> </p>
            <h6>3<sup>rd</sup> Place $100</h6><p class="winner"><span class="project">MHGen Calc </span> By: <span class='studentPresentor'>C.j. Pecor, Matt Reimann, Aaron Longchamp, McKenna Todd, Cuong Lai, Nel Korajkic, Chris Sandvik</span> </p></section>


        <section class="col-md-4"><h3>Intermediate Projects<span class="smallText block"> (42 projects, 61 students)</span></h3><h4>1<sup>st</sup> Place $300</h4><p class="winner"><span class="project">Raycaster</span> By: <span class='studentPresentor'>Jacob&nbsp;Wunder</span> </p><h5>2<sup>rd</sup> Place $200</h5><p class="winner"><span class="project">Solar Sound</span> By: <span class='studentPresentor'>Benjamin&nbsp;Jewkes</span> </p><h6>3<sup>rd</sup> Place $100</h6><p class="winner"><span class="project">Self-Archiving Rights Checker for Faculty Publications</span> By: <span class='studentPresentor'>Lynda&nbsp;Howell</span> </p></section><section class="col-md-4"><h3>Intermediate Web Design<span class="smallText block"> (38 projects, 67 students)</span></h3><h4>1<sup>st</sup> Place $300</h4><p class="winner"><span class="project">Mowmo - Lawn Care Management</span> By: <span class='studentPresentor'>Ryan&nbsp;Berliner</span> </p><h5>2<sup>rd</sup> Place $200</h5><p class="winner"><span class="project">Personal Portfolio</span> By: <span class='studentPresentor'>Nicholas&nbsp;Agel</span> </p><h6>3<sup>rd</sup> Place $100</h6><p class="winner"><span class="project">Automated Stock Trading platform</span> By: <span class='studentPresentor'>Tylor&nbsp;Mayfield</span> </p></section><section class="col-md-4"><h3>Beginner Programming<span class="smallText block"> (30 projects, 32 students)</span></h3><h4>1<sup>st</sup> Place $300</h4><p class="winner"><span class="project">Cryptography Program that decrypts Caesar Cipher/Monoalphabetic substitution cipher</span> By: <span class='studentPresentor'>Rohit&nbsp;Nawani, Ken&nbsp;Liu</span> </p><h5>2<sup>rd</sup> Place $200</h5><p class="winner"><span class="project">Societal and Economic Implications of Globalization and Automation</span> By: <span class='studentPresentor'>Clark&nbsp;Deng</span> </p><h6>3<sup>rd</sup> Place $100</h6><p class="winner"><span class="project">Tobacco use in America</span> By: <span class='studentPresentor'>Chris&nbsp;Aiello</span> </p></section><section class="col-md-4"><h3>Beginner Web Design<span class="smallText block"> (28 projects, 53 students)</span></h3><h4>1<sup>st</sup> Place $300</h4><p class="winner"><span class="project">So Picture This... Photography</span> By: <span class='studentPresentor'>Sean&nbsp;Miller</span> </p><h5>2<sup>rd</sup> Place $200</h5><p class="winner"><span class="project">Silicon Surgeon</span> By: <span class='studentPresentor'>Liam&nbsp;Beliveau, Phillip&nbsp;Nguyen</span> </p><h6>3<sup>rd</sup> Place $100</h6><p class="winner"><span class="project">Creative Writing Portfolio Website (CS008 Final Project)</span> By: <span class='studentPresentor'>Sarah&nbsp;Lewis</span> </p></section>
        <section class="col-md-4">
            <h3>People's Choice Award</h3>
            <h4>1<sup>st</sup> Place $300</h4>
            <p class="winner"><span class='project'>Creative Writing</span> <span class='studentPresentor'>By: Sarah Lewis</span>
            </p>
            <h5>2<sup>nd</sup> Place $200</h5>
            <p class="winner"><span class='project'>CCTV Mobile App</span> <span class='studentPresentor'>By: Nicholas Agel, 
                    Jason Lau, 
                    Duncan Whitaker, 
                    Jason Hammel, 
                    Lily Nguyen</span></p>
            <h6>3<sup>rd</sup> Place $100</h6>
            <p class="winner"><span class='project'>UVM Outing Club Trip Sign-Up and Management System</span> <span class='studentPresentor'>By: Matt Baris, 
                    Brian Colombini</span></p>

        </section>

        <section class="col-md-4">
            <h3>Random Hour Prizes</h3>
            <h5>1:10 PM $50</h5>
            <p class="winner"><span class='studentPresentor'>CS 142 Design Concept By: John Burke</span></p>
            <h5>2:20 PM PM $50</h5>
            <p class="winner"><span class='studentPresentor'>10,000 words or less CS008 Final By: Kyle Mac</span></p>
            <h5>3:40 PM $50</h5>
            <p class="winner"><span class='studentPresentor'>Sustainable Energy By: Dominic Eneji</span></p>

        </section>

    </div> <!-- end row -->
    <p class="disclaimer">
        <sup>*</sup>Prizes will be announced at the conclusion of the fair.
        Prize amounts are subject to change based on funding.
        Prizes awarded as UVM gift card.
    </p>


</section> <!-- main -->

<footer class="col-md-12">
    <p>Website design and event coordination provided By: the Society of UVM Women in Computer Science</p>
    <p>University of Vermont, Computer Science Department, 351 Votey, (802) 656 - 3330</p>
    <p style="text-align: center;"><a href="//csfair.w3.uvm.edu/2013/">CS Fair 2013</a> | <a href="//csfair.w3.uvm.edu/2014/">CS Fair 2014</a> | <a href="//csfair.w3.uvm.edu/2015/">CS Fair 2015</a> </p>
    <p style="float:right; font-size:50%;">[<a href="vote/results.php">Admin</a>]</p>
</footer>

</section> <!-- page-wrap -->
</body>
</html>