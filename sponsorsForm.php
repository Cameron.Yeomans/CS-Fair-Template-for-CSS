<?php
include ("top.php");
?>

<section id="main">
    <div class="row">
        <h1 class="page-title">Sponsorship Form</h1>
        <hr/>
    </div>

    <div class="row">
        <div class="col-md-6">
            <h3>Information</h3>
            <p>Sponsors will have their company logos posted on the CS Fair website and will be given table space (if requested) at the Fair to display their company information and paraphernalia. Sponsors are encouraged to send at two or more representatives to act as a judges. Many sponsors send a person to just walk around. Sponsorship is free, but we do welcome donations of any amount. We cannot have the fair without help.</p>
        </div>
        <div class="col-md-6">
            <h3>Process</h3>
            <ol>
                <li class="complete">Fill out required fields.</li>
                <li class="complete">Submit form</li>
                <li>Check mail and confirm sponsorship</li>
                <li>We will contact you and approve your submission. At this point your data will be made  live on the web site.</li>
                <li>We will send a link to update your information.</li>
                <li>You can then update your judges and other information.</li>
                <li>Join us at the Fair!</li>
            </ol>   
        </div>
    </div>
    <div id="errors" class="row">
        <h1>Please fix these mistakes:</h1>
        <ol>
            <li>Please enter your Company's Name</li>
        </ol>
    </div>
    <div class="row">
        <div class="col-md-12">

            <form action=""
                  enctype="multipart/form-data"
                  method="post"
                  id="frmRegister"
                  role="form">


                <fieldset class="contact">
                    <legend>Company Information <span class="right gray font-size-small"><span class="red font-size-medium-large">*</span>required</span></legend>
                    <div id="contactWrapper">

                        <div class="form-group mistake">
                            <label class="required" for="txtCompanyName">Company Name <span class="red">*</span></label>
                            <input id ="txtCompanyName"
                                   name="txtCompanyName"
                                   class="form-control"
                                   type="text"
                                   maxlength="255"
                                   value=""
                                   placeholder="Enter your company name"
                                   autofocus
                                   onfocus="this.select()"
                                   tabindex="10"
                                   required="required">

                            <!-- displays 'x' icon if an error was processed -->
                        </div>

                        <div class="form-group ">
                            <label class="required" for="txtCompanySite">Company Website (http://www.example.com) <span class="red">*</span></label>
                            <input id ="txtCompanySite"
                                   name="txtCompanySite"
                                   class="form-control"
                                   type="text"
                                   maxlength="255"
                                   value=""
                                   placeholder="Enter your company's website URL"
                                   onfocus="this.select()"
                                   tabindex="20"
                                   required="required">

                            <!-- displays 'x' icon if an error was processed -->
                        </div>

                        <div class="form-group">
                            <label for="imgCompanyLogo">Company Logo 
                            </label>
                            <input type='file' onchange="readCompURL(this);" tabindex="30" 
                                   class="element text large" 
                                   id="imgCompanyLogo"
                                   name="imgCompanyLogo">
                            <img id="compLogo" src="#" alt="" style="width: 150px;">


                            <input type='hidden' name='MAX_FILE_SIZE' value='90000000' >
                        </div>

                        <div class="form-group ">
                            <label class="required" for="txtContactName">Contact Name (full name please) <span class="red">*</span></label>
                            <input id ="txtContactName"
                                   name="txtContactName"
                                   class="form-control"
                                   type="text"
                                   maxlength="255"
                                   value=""
                                   placeholder="Enter your contact's full name"
                                   onfocus="this.select()"
                                   tabindex="40"
                                   required="required">
                            <!-- displays 'x' icon if an error was processed -->
                        </div>

                        <div class="form-group ">
                            <label class="required" for="txtContactEmail">Contact Email <span class="red">*</span></label>
                            <input id ="txtContactEmail"
                                   name="txtContactEmail"
                                   class="form-control"
                                   type="email"
                                   maxlength="255"
                                   value=""
                                   placeholder="Enter your email address"
                                   onfocus="this.select()"
                                   tabindex="50"
                                   required="required">
                            <!-- displays 'x' icon if an error was processed -->
                        </div>

                        <div class="form-group ">
                            <label class="required" for="txtContactPhone">Contact Phone Number (xxx-xxx-xxxx) <span class="red">*</span></label>
                            <input id ="txtContactPhone"
                                   name="txtContactPhone"
                                   class="form-control"
                                   type="text"
                                   maxlength="255"
                                   value=""
                                   placeholder="Enter your phone number"
                                   onfocus="this.select()"
                                   tabindex="60"
                                   required="required">
                            <!-- changed type to text from tel pattern="([0-9]{3}\-){2}[0-9]{4}" -->

                            <!-- displays 'x' icon if an error was processed -->
                        </div>

                        <div class="form-group ">
                            <label class="required" for="chkRequestTable">
                                <input id ="chkRequestTable"
                                       name="chkRequestTable"
                                       class="form-control"
                                       type="checkbox"                          
                                       value="table" 
                                       tabindex="65"> Request Table</label>
                        </div>

                        <div class="form-group">
                            <p>Sponsorship Level <span class="red">*</span></p>
                            <div class="sponsorship-legend">


                                <label class="checkbox-inline">Catamount (1500 +)</label>
                                <label class="checkbox-inline">Gold (750 to 1499) </label>
                                <label class="checkbox-inline">Silver (250 to 749)</label>
                                <label class="checkbox-inline">Bronze (0 to 249)</label>

                            </div>
                            <div>
                                <label class="required" for="txtContactEmail">Amount <span class="red">*</span></label>
                                <input id ="txtSponsorAmount"
                                       class="form-control" 
                                       name="txtSponserAmount" 
                                       type="text" 
                                       maxlength="255" 
                                       value=""
                                       placeholder="Enter cash donation amount here"
                                       onfocus="this.select()"
                                       tabindex="75"
                                       pattern="[0-9]*">
                            </div>
                        </div>

                        <input type="hidden" name="key1" value="-1">
                        <input type="hidden" name="key2" value="-1">
                    </div>
                </fieldset>

                <fieldset class="contact judgesWrapper">
                    <legend>Judges Information</legend>
                    <p>Sponsors should send one or more (better :) representatives to be a judge for the CS Fair. They will be assigned to advanced or intermediate level judging unless someone requests to judge the beginner level.</p>
                    <div id="judgesContactWrapper">
                        <fieldset id="judge0" class="contact judges">
                            <legend>First Judges Information</legend>

                            <div id="judgesContactWrapper0">

                                <div class="form-group ">
                                    <label for="txtJudgesFirstName0">First Name </label>
                                    <input id ="txtJudgesFirstName0"
                                           name="txtJudgesFirstName(0)"
                                           class="form-control"
                                           type="text"
                                           maxlength="257"
                                           value=""
                                           placeholder="Judges first name"
                                           onfocus="this.select()"
                                           tabindex="400">
                                    <!-- displays 'x' icon if an error was processed -->
                                </div>


                                <div class="form-group ">
                                    <label for="txtJudgesLastName0">Last Name </label>
                                    <input id ="txtJudgesLastName0"
                                           name="txtJudgesLastName(0)"
                                           class="form-control"
                                           type="text"
                                           maxlength="258"
                                           value=""
                                           placeholder="Judges last name"
                                           onfocus="this.select()"
                                           tabindex="410">
                                    <!-- displays 'x' icon if an error was processed -->
                                </div>


                                <div class="form-group ">
                                    <label for="txtJudgesTitle0">Job Title </label>
                                    <input id ="txtJudgesTitle0"
                                           name="txtJudgesTitle(0)"
                                           class="form-control"
                                           type="text"
                                           maxlength="258"
                                           value=""
                                           placeholder="Job title"
                                           onfocus="this.select()"
                                           tabindex="415">
                                    <!-- displays 'x' icon if an error was processed -->
                                </div>

                                <div class="form-group">
                                    <label for="txtJudgesPhoto0">Judge's Photo</label>
                                    <input type='file' onchange='readJudge0URL(this);'                                                    name="txtJudgesPhoto0"
                                           id ="txtJudgesPhoto0"
                                           class="element text large"
                                           tabindex="420">
                                    <img id="j0Logo" src="#" alt=""  style="width: 150px;">   
                                </div>                           

                                <div class="form-group ">
                                    <label for="txtJudgesEmail0">Judges Email (will not be posted)</label>
                                    <input id ="txtJudgesEmail0"
                                           name="txtJudgesEmail(0)"
                                           class="form-control"
                                           type="email"
                                           maxlength="265"
                                           value=""
                                           placeholder="Email address"
                                           onfocus="this.select()"
                                           tabindex="430">
                                    <!-- displays 'x' icon if an error was processed -->
                                </div>

                                <div class="form-group">
                                    <label for="txtJudgeBio0" class="judgesbio">Bio</label>
                                    <textarea class="form-control judgesbio" id="txtJudgeBio0" name="txtJudgeBio(0)" tabindex="450"
                                              onfocus="this.select()"></textarea>
                                </div>
                                <input type="button" class="btn btn-default" id="btnAdd0" value="+ Add a Judge" tabindex="460" onclick="ja
                                        vascript:showNextJudge(0);">
                            </div></fieldset>                                        <fieldset id="judge1" class="contact judges">
                            <legend>Second Judges Information</legend>

                            <div id="judgesContactWrapper1">

                                <div class="form-group ">
                                    <label for="txtJudgesFirstName1">First Name </label>
                                    <input id ="txtJudgesFirstName1"
                                           name="txtJudgesFirstName(1)"
                                           class="form-control"
                                           type="text"
                                           maxlength="257"
                                           value=""
                                           placeholder="Judges first name"
                                           onfocus="this.select()"
                                           tabindex="500">
                                    <!-- displays 'x' icon if an error was processed -->
                                </div>


                                <div class="form-group ">
                                    <label for="txtJudgesLastName1">Last Name </label>
                                    <input id ="txtJudgesLastName1"
                                           name="txtJudgesLastName(1)"
                                           class="form-control"
                                           type="text"
                                           maxlength="258"
                                           value=""
                                           placeholder="Judges last name"
                                           onfocus="this.select()"
                                           tabindex="510">
                                    <!-- displays 'x' icon if an error was processed -->
                                </div>


                                <div class="form-group ">
                                    <label for="txtJudgesTitle1">Job Title </label>
                                    <input id ="txtJudgesTitle1"
                                           name="txtJudgesTitle(1)"
                                           class="form-control"
                                           type="text"
                                           maxlength="258"
                                           value=""
                                           placeholder="Job title"
                                           onfocus="this.select()"
                                           tabindex="515">
                                    <!-- displays 'x' icon if an error was processed -->
                                </div>

                                <div class="form-group">
                                    <label for="txtJudgesPhoto1">Judge's Photo</label>
                                    <input type='file' onchange='readJudge1URL(this);'                                                    name="txtJudgesPhoto1"
                                           id ="txtJudgesPhoto1"
                                           class="element text large"
                                           tabindex="520">
                                    <img id="j1Logo" src="#" alt=""  style="width: 150px;">   
                                </div>                           

                                <div class="form-group ">
                                    <label for="txtJudgesEmail1">Judges Email (will not be posted)</label>
                                    <input id ="txtJudgesEmail1"
                                           name="txtJudgesEmail(1)"
                                           class="form-control"
                                           type="email"
                                           maxlength="265"
                                           value=""
                                           placeholder="Email address"
                                           onfocus="this.select()"
                                           tabindex="530">
                                    <!-- displays 'x' icon if an error was processed -->
                                </div>

                                <div class="form-group">
                                    <label for="txtJudgeBio1" class="judgesbio">Bio</label>
                                    <textarea class="form-control judgesbio" id="txtJudgeBio1" name="txtJudgeBio(1)" tabindex="550"
                                              onfocus="this.select()"></textarea>
                                </div>
                                <input type="button" class="btn btn-default" id="btnAdd1" value="+ Add a Judge" tabindex="560" onclick="ja
                                        vascript:showNextJudge(1);">
                            </div></fieldset>                                        <fieldset id="judge2" class="contact judges">
                            <legend>Third Judges Information</legend>

                            <div id="judgesContactWrapper2">

                                <div class="form-group ">
                                    <label for="txtJudgesFirstName2">First Name </label>
                                    <input id ="txtJudgesFirstName2"
                                           name="txtJudgesFirstName(2)"
                                           class="form-control"
                                           type="text"
                                           maxlength="257"
                                           value=""
                                           placeholder="Judges first name"
                                           onfocus="this.select()"
                                           tabindex="600">
                                    <!-- displays 'x' icon if an error was processed -->
                                </div>


                                <div class="form-group ">
                                    <label for="txtJudgesLastName2">Last Name </label>
                                    <input id ="txtJudgesLastName2"
                                           name="txtJudgesLastName(2)"
                                           class="form-control"
                                           type="text"
                                           maxlength="258"
                                           value=""
                                           placeholder="Judges last name"
                                           onfocus="this.select()"
                                           tabindex="610">
                                    <!-- displays 'x' icon if an error was processed -->
                                </div>


                                <div class="form-group ">
                                    <label for="txtJudgesTitle2">Job Title </label>
                                    <input id ="txtJudgesTitle2"
                                           name="txtJudgesTitle(2)"
                                           class="form-control"
                                           type="text"
                                           maxlength="258"
                                           value=""
                                           placeholder="Job title"
                                           onfocus="this.select()"
                                           tabindex="615">
                                    <!-- displays 'x' icon if an error was processed -->
                                </div>

                                <div class="form-group">
                                    <label for="txtJudgesPhoto2">Judge's Photo</label>
                                    <input type='file' onchange='readJudge2URL(this);'                                                    name="txtJudgesPhoto2"
                                           id ="txtJudgesPhoto2"
                                           class="element text large"
                                           tabindex="620">
                                    <img id="j2Logo" src="#" alt=""  style="width: 150px;">   
                                </div>                           

                                <div class="form-group ">
                                    <label for="txtJudgesEmail2">Judges Email (will not be posted)</label>
                                    <input id ="txtJudgesEmail2"
                                           name="txtJudgesEmail(2)"
                                           class="form-control"
                                           type="email"
                                           maxlength="265"
                                           value=""
                                           placeholder="Email address"
                                           onfocus="this.select()"
                                           tabindex="630">
                                    <!-- displays 'x' icon if an error was processed -->
                                </div>

                                <div class="form-group">
                                    <label for="txtJudgeBio2" class="judgesbio">Bio</label>
                                    <textarea class="form-control judgesbio" id="txtJudgeBio2" name="txtJudgeBio(2)" tabindex="650"
                                              onfocus="this.select()"></textarea>
                                </div>
                                <input type="button" class="btn btn-default" id="btnAdd2" value="+ Add a Judge" tabindex="660" onclick="ja
                                        vascript:showNextJudge(2);">
                            </div></fieldset>                                        <fieldset id="judge3" class="contact judges">
                            <legend>Fourth Judges Information</legend>

                            <div id="judgesContactWrapper3">

                                <div class="form-group ">
                                    <label for="txtJudgesFirstName3">First Name </label>
                                    <input id ="txtJudgesFirstName3"
                                           name="txtJudgesFirstName(3)"
                                           class="form-control"
                                           type="text"
                                           maxlength="257"
                                           value=""
                                           placeholder="Judges first name"
                                           onfocus="this.select()"
                                           tabindex="700">
                                    <!-- displays 'x' icon if an error was processed -->
                                </div>


                                <div class="form-group ">
                                    <label for="txtJudgesLastName3">Last Name </label>
                                    <input id ="txtJudgesLastName3"
                                           name="txtJudgesLastName(3)"
                                           class="form-control"
                                           type="text"
                                           maxlength="258"
                                           value=""
                                           placeholder="Judges last name"
                                           onfocus="this.select()"
                                           tabindex="710">
                                    <!-- displays 'x' icon if an error was processed -->
                                </div>


                                <div class="form-group ">
                                    <label for="txtJudgesTitle3">Job Title </label>
                                    <input id ="txtJudgesTitle3"
                                           name="txtJudgesTitle(3)"
                                           class="form-control"
                                           type="text"
                                           maxlength="258"
                                           value=""
                                           placeholder="Job title"
                                           onfocus="this.select()"
                                           tabindex="715">
                                    <!-- displays 'x' icon if an error was processed -->
                                </div>

                                <div class="form-group">
                                    <label for="txtJudgesPhoto3">Judge's Photo</label>
                                    <input type='file' onchange='readJudge3URL(this);'                                                    name="txtJudgesPhoto3"
                                           id ="txtJudgesPhoto3"
                                           class="element text large"
                                           tabindex="720">
                                    <img id="j3Logo" src="#" alt=""  style="width: 150px;">   
                                </div>                           

                                <div class="form-group ">
                                    <label for="txtJudgesEmail3">Judges Email (will not be posted)</label>
                                    <input id ="txtJudgesEmail3"
                                           name="txtJudgesEmail(3)"
                                           class="form-control"
                                           type="email"
                                           maxlength="265"
                                           value=""
                                           placeholder="Email address"
                                           onfocus="this.select()"
                                           tabindex="730">
                                    <!-- displays 'x' icon if an error was processed -->
                                </div>

                                <div class="form-group">
                                    <label for="txtJudgeBio3" class="judgesbio">Bio</label>
                                    <textarea class="form-control judgesbio" id="txtJudgeBio3" name="txtJudgeBio(3)" tabindex="750"
                                              onfocus="this.select()"></textarea>
                                </div>
                                <input type="button" class="btn btn-default" id="btnAdd3" value="+ Add a Judge" tabindex="760" onclick="ja
                                        vascript:showNextJudge(3);">
                            </div></fieldset>                                        <fieldset id="judge4" class="contact judges">
                            <legend>Fifth Judges Information</legend>

                            <div id="judgesContactWrapper4">

                                <div class="form-group ">
                                    <label for="txtJudgesFirstName4">First Name </label>
                                    <input id ="txtJudgesFirstName4"
                                           name="txtJudgesFirstName(4)"
                                           class="form-control"
                                           type="text"
                                           maxlength="257"
                                           value=""
                                           placeholder="Judges first name"
                                           onfocus="this.select()"
                                           tabindex="800">
                                    <!-- displays 'x' icon if an error was processed -->
                                </div>


                                <div class="form-group ">
                                    <label for="txtJudgesLastName4">Last Name </label>
                                    <input id ="txtJudgesLastName4"
                                           name="txtJudgesLastName(4)"
                                           class="form-control"
                                           type="text"
                                           maxlength="258"
                                           value=""
                                           placeholder="Judges last name"
                                           onfocus="this.select()"
                                           tabindex="810">
                                    <!-- displays 'x' icon if an error was processed -->
                                </div>


                                <div class="form-group ">
                                    <label for="txtJudgesTitle4">Job Title </label>
                                    <input id ="txtJudgesTitle4"
                                           name="txtJudgesTitle(4)"
                                           class="form-control"
                                           type="text"
                                           maxlength="258"
                                           value=""
                                           placeholder="Job title"
                                           onfocus="this.select()"
                                           tabindex="815">
                                    <!-- displays 'x' icon if an error was processed -->
                                </div>

                                <div class="form-group">
                                    <label for="txtJudgesPhoto4">Judge's Photo</label>
                                    <input type='file' onchange='readJudge4URL(this);'                                                    name="txtJudgesPhoto4"
                                           id ="txtJudgesPhoto4"
                                           class="element text large"
                                           tabindex="820">
                                    <img id="j4Logo" src="#" alt=""  style="width: 150px;">   
                                </div>                           

                                <div class="form-group ">
                                    <label for="txtJudgesEmail4">Judges Email (will not be posted)</label>
                                    <input id ="txtJudgesEmail4"
                                           name="txtJudgesEmail(4)"
                                           class="form-control"
                                           type="email"
                                           maxlength="265"
                                           value=""
                                           placeholder="Email address"
                                           onfocus="this.select()"
                                           tabindex="830">
                                    <!-- displays 'x' icon if an error was processed -->
                                </div>

                                <div class="form-group">
                                    <label for="txtJudgeBio4" class="judgesbio">Bio</label>
                                    <textarea class="form-control judgesbio" id="txtJudgeBio4" name="txtJudgeBio(4)" tabindex="850"
                                              onfocus="this.select()"></textarea>
                                </div>
                            </div>
                        </fieldset>



                        <fieldset class="buttons">
                            <legend>After receiving your submission the Computer
                                Science Department will be in contact with you.</legend>

                            <input type="submit" id="btnSubmit" name="btnSubmit" value="Submit" tabindex="991" class="button btn btn-primary btn-block btn-lg">
                        </fieldset>

                    </div>
                </fieldset> <!-- contact wrapper -->
            </form>
        </div> <!-- end col-md-12 -->
    </div> <!-- end row -->
</section> <!-- end main -->

<?php
include ("footer.php");
?>


</body>
</html>
